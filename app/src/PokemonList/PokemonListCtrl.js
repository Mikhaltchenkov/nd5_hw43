'use strict';

pokemonApp.controller('PokemonListCtrl', function($scope, PokemonsService, BerriesService, $q) {

	$scope.pokemonListLoaded = false;
	$scope.berryListLoaded = false;
	
	$q.all({pokemons: PokemonsService.getPokemons(), berries: BerriesService.getBerries()}).then(function(values) {
        $scope.pokemons = values.pokemons.data.data;
        $scope.pokemonListLoaded = true;
		$scope.berries = values.berries.data.data;
        $scope.berryListLoaded = true;
    });
	
});
