angular
    .module('PokemonApp')
    .factory('BerriesService', function($http) {
			let appUrl = 'https://api.backendless.com/v1/data/';
            return {

                getBerries: function() {
                    //return $http.get('http://pokeapi.co/api/v2/berry/?limit=5');
					return $http({
                      method: 'GET',
                      url: appUrl + 'berry'
                  });
                }

            }

        }

    );
