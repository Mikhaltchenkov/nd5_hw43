angular
    .module('PokemonApp')
    .factory('PokemonsService', function($http) {
            let appUrl = 'https://api.backendless.com/v1/data/';
            return {


                getPokemons: function() {
                  return $http({
                      method: 'GET',
                      url: appUrl + 'pokemon'
                  });
                },

                getPokemon: function(pokemonId) {
                  return $http({
                      method: 'GET',
                      url: appUrl + 'pokemon/' + pokemonId
                  });
                },

                createPokemon: function(pokemonData) {
                    return $http({
                        method: 'POST',
                        url: appUrl + 'pokemon',
                        data: pokemonData
                    });
                },

                editPokemon: function(pokemonData) {
                    return $http({
                        method: 'PUT',
                        url: appUrl + 'pokemon',
                        data: pokemonData
                    });
                },

                deletePokemon: function(pokemonId) {
                    return $http({
                        method: 'DELETE',
                        url: appUrl + 'pokemon/' + pokemonId
                    });
                }

            }

        }

    );
